using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Reflection;

[CustomEditor(typeof(Game))]
public class GameEditor : Editor
{
	#region Constants
	
	
	static readonly float PARAMETER_INDENTATION = 28;
	
	
	#endregion
	
	
	#region Fields
	
	
	Game instance;
	
	
	#endregion
	
	
	#region Editor event handlers
	
	
	public void OnEnable()
	{		
		instance = (Game)target;
	}
	
	public override void OnInspectorGUI()
	{
		if (instance == null)
			return;
		
		DrawDefaultInspector();
		DrawRemovedObjectNamesSection();
		DrawAddedObjectsSection();
		DrawFlagsSection();
		
		if (GUI.changed || Application.isPlaying)
			EditorUtility.SetDirty(target);
	}
	
	
	#endregion
	
	
	#region Removed object names
	
	
	void DrawRemovedObjectNamesSection()
	{
		// section title / foldout
		instance.foldoutRemovedSection = EditorGUILayout.Foldout(
			instance.foldoutRemovedSection, "Removed Object Names");
		
		if (!instance.foldoutRemovedSection)
			return;
		
		Game.SceneGOName[] removedNames = instance.RemovedFromScene;
		
		if ((removedNames == null) || (removedNames.Length <= 0))
		{
			EditorGUILayout.BeginHorizontal();
			GUILayout.Space(PARAMETER_INDENTATION);
			EditorGUILayout.LabelField("None");
			EditorGUILayout.EndHorizontal();
		}
		else
		{
			string currentScene = Application.loadedLevelName;
			
			foreach (Game.SceneGOName n in removedNames)
			{				
				EditorGUILayout.BeginHorizontal();
				GUILayout.Space(PARAMETER_INDENTATION);
				
				if (n.sceneId != currentScene)
					GUI.enabled = false;
				
				EditorGUILayout.LabelField("Removed name from scene '"
					+ n.sceneId + "'");
				
				if (n.sceneId != currentScene)
					GUI.enabled = true;
				
				GUI.enabled = false;	// yet to make this field editable
				EditorGUILayout.TextField(n.goName);
				GUI.enabled = true;
				
				EditorGUILayout.EndHorizontal();
			}
		}
	}
	
	
	#endregion
	
	
	#region Added objects
	
	
	void DrawAddedObjectsSection()
	{
		// section title / foldout
		instance.foldoutAddedSection = EditorGUILayout.Foldout(
			instance.foldoutAddedSection, "Added Objects");
		
		if (!instance.foldoutAddedSection)
			return;
		
		Game.SceneGO[] addedNames = instance.AddedToScene;
		
		if ((addedNames == null) || (addedNames.Length <= 0))
		{
			EditorGUILayout.BeginHorizontal();
			GUILayout.Space(PARAMETER_INDENTATION);
			EditorGUILayout.LabelField("None");
			EditorGUILayout.EndHorizontal();
		}
		else
		{
			string currentScene = Application.loadedLevelName;
			
			foreach (Game.SceneGO o in addedNames)
			{
				EditorGUILayout.BeginHorizontal();
				GUILayout.Space(PARAMETER_INDENTATION);
				
				if (o.sceneId != currentScene)
					GUI.enabled = false;
				
				EditorGUILayout.LabelField("Added object to scene '" +
					o.sceneId + "'");
				
				if (o.sceneId != currentScene)
					GUI.enabled = true;
				
				GUI.enabled = false;	// yet to make this field editable
				EditorGUILayout.ObjectField(o.go, typeof(GameObject), true, 
					GUILayout.MinHeight(19));
				GUI.enabled = true;
				
				EditorGUILayout.EndHorizontal();
			}
		}
	}
	
	
	#endregion
	
	
	#region Flags
	
	
	void DrawFlagsSection()
	{
		// section title / foldout
		instance.foldoutFlagsSection = EditorGUILayout.Foldout(
			instance.foldoutFlagsSection, "Global Flags");
		
		if (!instance.foldoutFlagsSection)
			return;
		
		string[] flags = instance.Flags;
		
		if ((flags == null) || (flags.Length <= 0))
		{
			EditorGUILayout.BeginHorizontal();
			GUILayout.Space(PARAMETER_INDENTATION);
			EditorGUILayout.LabelField("None");
			EditorGUILayout.EndHorizontal();
		}
		else
		{
			foreach (string f in flags)
			{
				EditorGUILayout.BeginHorizontal();
				GUILayout.Space(PARAMETER_INDENTATION);
				
				EditorGUILayout.LabelField(f);
				
				GUI.enabled = false;	// yet to make this field editable
				EditorGUILayout.Toggle(true);
				GUI.enabled = true;
				
				EditorGUILayout.EndHorizontal();
			}
		}
	}
	
	
	#endregion
}
