using UnityEngine;
using System;

public class FixBridge : SingleIScript
{
	public string bridgeName;
	
	protected override void OnSuccess()
	{		
		GameObject bridge = Helpers.GetGameObject(bridgeName);
		
		iTween.RotateBy(gameObject, 
			iTween.Hash("amount",new Vector3(0, 1f, 0), "time",1f));
		
		iTween.RotateBy(bridge,
			iTween.Hash("amount",new Vector3(0, 0, -0.25f), "time",1f,
			"easeType","linear",
			"oncomplete","UpdateNavMesh", "oncompletetarget",gameObject));
		
		Helpers.ChangeLayer(bridge, AdventureUtility.LAYER_DEFAULT, true);
		Helpers.Game.RemoveObjectsWithNameFromScene(bridge);
		
		Interactable.GetInteraction(this).AvailableInScene = false;
	}
	
	void UpdateNavMesh()
	{
		Helpers.UpdateNavMesh();
	}
	
	protected override void OnOutOfRange()
	{
	}
}
